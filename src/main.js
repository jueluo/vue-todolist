import Vue from "vue";
import App from "./App";
import "./assets/css/scss/style.scss";
import * as localforage from "localforage";

Vue.config.productionTip = false;

new Vue({
    el: "#app",
    components: {
        App
    },
    template: "<App/>",
    methods: {
        // 连接和创建数据库
    },
    // 实例创建完成后被立即调用
    created: function () {
        // localforage配置
        localforage.config({
            driver: localforage.INDEXEDDB, // Force WebSQL; same as using setDriver()
            name: 'todolist',
            version: 1.0,
            size: 4980736, // Size of database, in bytes. WebSQL-only for now.
            storeName: 'app', // Should be alphanumeric, with underscores.
            description: 'a todolist app'
        });

        // 确定异步驱动程序初始化过程是否已完成
        localforage.ready().then(function() {
            // 当 localforage 将指定驱动初始化完成时，此处代码运行
            console.log(localforage); // LocalStorage
        }).catch(function (e) {
            console.log(e); // `No available storage method found.`
            // 当没有可用的驱动时，`ready()` 将会失败
        });
    },
    // 实例被挂载后调用
    mounted: function () {
        // 连接和创建数据库
    }
});
