# CODING TODO

##### 释放您的大脑，即刻开始管理你的思路

将任务从您的脑海中移到任务清单上来重新找回清晰和宁静，让你从工作到娱乐都保持专注。

> 一个用vuejs完成的todolist，功能齐全。

数据存储在本地indexedDB

预览地址：http://jueluo.gitee.io/vue-todolist

## 预览图

![image](./screenshot/01.png)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
